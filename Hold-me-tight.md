# Hold me tight

Hold me tight, <br>

ev`ry night, <br>

by your side, <br>

you are my light.<br>

<br>

Halte mich fest, <br>

jede Nacht, <br>

an deiner Seite, <br>

du bist mein Licht.<br>