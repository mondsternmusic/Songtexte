# Kopflos

ohne arme und ohne beine<br>
ohne füße ohne hände<br>
ich bin kopflos<br>
bin kopflos<br>

kopflos kopflos kopflos<br>

ziellos<br>
ohne sinn und ohne weg<br>
ohne fahrt ohne steg<br>
ich bin ziellos<br>
bin ziellos<br>

ziellos kopflos ziellos<br>

farblos<br>
ohne blau und ohne grün<br>
ohne gelb ohne rot<br>
ich bin farblos<br>
bin farblos<br>

farblos ziellos farblos<br>

rastlos<br>
ohne haus und ohne heim<br>
ohne rast ohne ruh<br>
ich bin rastlos<br>
bin rastlos<br>

rastlos farblos rastlos<br>

zeitlos<br>
ohne stunden und ohne minuten<br>
ohne tage ohne jahre<br>
ich bin zeitlos<br>
bin zeitlos<br>

zeitlos rastlos zeitlos<br>

besitzlos<br>
ohne hab und ohne gut<br>
ohne auto ohne geld<br>
ich bin besitzlos<br>
bin besitzlos<br>

besitzlos zeitlos besitzlos<br>

lieblos<br>
ohne freude und ohne umarmung<br>
ohne schmerz ohne trauer<br>
ich bin lieblos<br>
bin lieblos<br>

lieblos besitzlos lieblos<br>

hilflos<br>
ohne drei und ohne zwei<br>
ohne zwei ohne eins<br>
ich bin hilflos<br>
bin hilflos<br>

hilflos lieblos hilflos<br>

leblos<br>
ohne luft und ohne herz<br>
ohne atem ohne schmerz<br>
ich bin leblos<br>
bin leblos<br>

leblos leblos leblos<br>