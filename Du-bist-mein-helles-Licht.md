# Du bist mein helles Licht

Am Wegesrand stehen die bunten Blumen, <br>

und daran Schuld hat die helle Sonne, <br>

da ich so gerne lese halte ich ein Buch, <br>

und machs mir bequem auf dem Teppich.<br>

<br>

Tränen sind ein Zeichen der Gefühle, <br>

wie sie doch vergeht die liebe Zeit, <br>

was mache ich, kann sie nicht vergessen, <br>

du umarmst mich, du bist mein helles Licht.<br>