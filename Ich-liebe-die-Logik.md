# Ich liebe die Logik

Ich liebe die Logik, <br>
und die Logik liebt mich, <br>
und gäbs die Logik nicht, <br>
dann gäbs mich auch nicht. <br>

Ich sitze in dem Auto, <br>
und fahre eine Strasse, <br>
am Wegesrand sind Bäume, <br>
Sträuche, Steine, Blätter.<br>

Ich liebe die Logik <br>
und fahre auf der Strasse, <br>
Ich sitze in dem Auto <br>
und die Logik liebt mich. <br>

Am Wegesrand sind Bäume,<br>
denn dann gäbs die Logik nicht. <br>
Sträuche, Steine, Blätter <br>
dann gäbs mich auch nicht<br>