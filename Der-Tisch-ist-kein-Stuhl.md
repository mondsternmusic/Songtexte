# Der Tisch ist kein Stuhl

Der Tisch ist kein Stuhl,<br>

und der Stuhl ist kein Tisch.<br>

Der Fisch ist kein Bär,<br>

und der Bär ist kein Fisch<br>